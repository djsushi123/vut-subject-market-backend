# VUT Subject Market Backend

## Installation

### IntelliJ

You can import the project directly from GitLab by using File -> New
-> Project from version control.
Gradle should automatically start building the project and downloading
the dependencies.


### Terminal / VScode

There exist some VScode plugins for gradle which allow you to
directly run Gradle tasks from VScode. If you wish to use the
terminal, run `gradle build` to build the project and import
all the dependencies.


## Running

To run the code, run the `gradle run` task, and it should
start the development server.

#### Auto-reload

To have auto-reloading of the .class files, you need to run
a second Gradle task simultaneously with the `run` task, and
that task is `gradle build -t`. The `-t` flag can be replaced by
`--continuous` and it tells Gradle to watch changes in the
source files and automatically build the project if some changes
occur.