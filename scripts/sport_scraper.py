from dataclasses import dataclass
import requests, json
from bs4 import BeautifulSoup
from pprint import pprint


url_base = 'https://www.vut.cz/studis/student.phtml?lang=0' 
url_sports = url_base + "&sn=zapis_sportu"
cookies = {'PHPSESSID': 'bulldog1-250634-1675976689-1170415-d0bb6aa24835bc6c6dbadb81fdb69dacdfb51f32', 'hash_uzivatele': '5acd39f807e655bdff058c6e70e847bf', 'portal_is_logged_in': '1'}
req = requests.get(url_sports, cookies=cookies)
soup = BeautifulSoup(req.text, "html.parser")

@dataclass
class SportTerm:
    name: str
    abbreviation: str
    day: str
    timeStart: str
    timeEnd: str
    week: str
    teachingType: str
    room: str
    teacher: str
    capacity: str
    note: str


sport_terms = []

if __name__ == '__main__':
    sports = soup.select('body > div.studis-layout > div.vut-layout-wrap.vut-layout-wrap-main > div.main > div > div')[0].select('a')
    # pprint(sports[0])
    for sport in sports:
        url = url_base + sport['href']
        soup2 = BeautifulSoup(requests.get(url_base + sport['href'].replace('?', '&'), cookies=cookies).text, "html.parser")
        for day in soup2.select('div', _class='den'):
            for rows in day.select('div' ,_class='radky'):
                for row in rows.select('div', _class='radek'):
                    # pprint(row.select('div').attrs.get("data-value", None))
                    content = row.attrs.get('data-content', None)
                    if content is not None:
                        print('==============')
                        soup3 = BeautifulSoup(content, 'html.parser')
                        name = soup3.select_one('h5')
                        note = soup3.select('tr')[-1].select_one('td')
                        table_data = soup3.select('td')
                        sport_terms.append(SportTerm(
                            name = name.text,
                            abbreviation = table_data[0].text,
                            day = table_data[1].text,
                            timeStart = table_data[2].text.split('–')[0],
                            timeEnd = table_data[2].text.split('–')[-1],
                            week = table_data[3].text,
                            teachingType = table_data[4].text,
                            room = table_data[5].text,
                            teacher = table_data[6].text,
                            capacity = table_data[7].text.split('/')[-1],
                            note = note.text if note is not None else '',
                        ))
                        pprint(sport_terms[-1])
    json_string = json.dumps([sport.__dict__ for sport in sport_terms])
    with open("scripts/sport_terms.json", "w") as file:
        file.write(json_string)

    