@file:Suppress("NonAsciiCharacters")

package com.hello

import com.hello.data.dto.SportTermDto
import com.hello.data.local.entity.SportTermEntity
import com.hello.data.local.table.SportTermsTable
import com.hello.data.local.table.SubjectsTable
import com.hello.data.local.table.TermsTable
import com.hello.data.local.table.UsersTable
import com.hello.data.mapper.toModel
import com.hello.domain.model.SportTerm
import com.hello.plugin.configureRouting
import com.hello.vut_subject_market_backend.BuildConfig
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.decodeFromJsonElement
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

fun main() {
    embeddedServer(Netty, port = 4321, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    install(ContentNegotiation) {
        json()
    }

    val db = Database.connect(
        url = "jdbc:mariadb://2042175058.kn.vutbr.cz:3306/vut_subject_market",
        driver = "org.mariadb.jdbc.Driver",
        user = BuildConfig.DB_USERNAME,
        password = BuildConfig.DB_PASSWORD
    )

    transaction {
        SchemaUtils.createMissingTablesAndColumns(SubjectsTable, TermsTable, UsersTable, SportTermsTable)
    }

    configureRouting()
}
