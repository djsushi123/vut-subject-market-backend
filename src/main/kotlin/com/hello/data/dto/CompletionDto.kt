package com.hello.data.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class CompletionDto {
    @SerialName("-") NONE,
    @SerialName("Za") CREDIT,
    @SerialName("ZaZk") CREDIT_EXAM,
    @SerialName("Zk") EXAM,
    @SerialName("Klz") CLASSIFIED_CREDIT,
//    @SerialName("Colloquium") COLLOQUIUM TODO
}
