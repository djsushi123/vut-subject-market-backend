package com.hello.data.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
enum class DayDto {
    @SerialName("Mon") MONDAY,
    @SerialName("Tue") TUESDAY,
    @SerialName("Wed") WEDNESDAY,
    @SerialName("Thu") THURSDAY,
    @SerialName("Fri") FRIDAY,
    @SerialName("Sat") SATURDAY,
    @SerialName("Sun") SUNDAY,
    @SerialName("") NONE
}
