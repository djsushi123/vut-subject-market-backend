package com.hello.data.dto

import kotlinx.serialization.Serializable

@Serializable
enum class FacultyDto {
    FIT,
    FEKT,
    ICV,
    FaVU,
    FSI,
    FP,
    FCH,
    CESA,
    USI,
    FAST,
    FA,
    CEITEC_VUT
}
