package com.hello.data.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class ObligationDto {
    @SerialName("P") MANDATORY,
    @SerialName("PV") COMPULSORY,
    @SerialName("PVA") COMPULSORY_ENGLISH,
    @SerialName("PVT") COMPULSORY_TECHNICAL,
    @SerialName("V") ELECTIVE
}
