package com.hello.data.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class SemesterDto {
    @SerialName("zimny") WINTER,
    @SerialName("letny") SUMMER,
}
