package com.hello.data.dto

import com.hello.data.serializer.LocalTimeSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import java.time.LocalTime

@Serializable
data class SportTermDto(
//    @SerialName("id") val id: Int,
    @SerialName("name") val name: String,
    @SerialName("abbreviation") val abbreviation: String,
    @SerialName("day") val day: DayDto,

    @SerialName("start_time")
    @Serializable(with = LocalTimeSerializer::class)
    val startTime: LocalTime,

    @SerialName("end_time")
    @Serializable(with = LocalTimeSerializer::class)
    val endTime: LocalTime,

    @SerialName("weeks") val weeks: String,
    @SerialName("teachingType") val teachingType: TeachingTypeDto,
    @SerialName("room") val room: String,
    @SerialName("teacher") val teacher: String,
    @SerialName("capacity") val capacity: String?,
    @SerialName("note") val note: String
)