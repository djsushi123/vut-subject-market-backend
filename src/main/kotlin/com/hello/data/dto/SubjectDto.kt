package com.hello.data.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SubjectDto(
    @SerialName("id") val id: Int,
    @SerialName("name") val name: String,
    @SerialName("abbreviation") val abbreviation: String,
    @SerialName("credits") val credits: Int,
    @SerialName("obligation") val obligation: ObligationDto,
    @SerialName("completion") val completion: CompletionDto,
    @SerialName("faculty") val faculty: FacultyDto,
    @SerialName("semester") val semester: SemesterDto,
    @SerialName("year") val year: Int
)