package com.hello.data.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class TeachingTypeDto {
    @SerialName("tv_exercise") TV_EXERCISE,
    @SerialName("tech_exercise") TECHNOLOGICAL_EXERCISE,
}

