package com.hello.data.dto

import com.hello.data.serializer.LocalTimeSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import java.time.LocalTime

@Serializable
data class TermDto(
    @SerialName("id") val id: Int,
    @SerialName("subject_id") val subjectId: Int,
    @SerialName("day") val day: DayDto,
    @SerialName("type") val type: TermTypeDto,
    @SerialName("weeks") val weeks: String,
    @SerialName("room") val room: String,

    @SerialName("start_time")
    @Serializable(with = LocalTimeSerializer::class)
    val startTime: LocalTime,

    @SerialName("end_time")
    @Serializable(with = LocalTimeSerializer::class)
    val endTime: LocalTime,

    @SerialName("capacity") val capacity: Int?,
    @SerialName("info") val info: String
)