package com.hello.data.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class TermTypeDto {
    @SerialName("exercise") EXERCISE,
    @SerialName("lecture") LECTURE,
    @SerialName("laboratory") LABORATORY,
    @SerialName("other") OTHER,
    @SerialName("comp.lab") COMPUTER_LABORATORY
}

