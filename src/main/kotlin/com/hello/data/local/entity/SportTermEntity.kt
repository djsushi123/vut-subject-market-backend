package com.hello.data.local.entity

import com.hello.data.local.table.SportTermsTable
import com.hello.data.local.table.TermsTable
import kotlinx.serialization.SerialName
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class SportTermEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<SportTermEntity>(SportTermsTable)

    var name by SportTermsTable.name
    var abbreviation by SportTermsTable.abbreviation
    var day by SportTermsTable.day
    var startTime by SportTermsTable.startTime
    var endTime by SportTermsTable.endTime
    var weeks by SportTermsTable.weeks
    var teachingType by SportTermsTable.teachingType
    var room by SportTermsTable.room
    var teacher by SportTermsTable.teacher
    var capacity by SportTermsTable.capacity
    var note by SportTermsTable.note
}