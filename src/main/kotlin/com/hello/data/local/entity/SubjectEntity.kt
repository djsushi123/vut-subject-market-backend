package com.hello.data.local.entity

import com.hello.data.local.table.SubjectsTable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class SubjectEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<SubjectEntity>(SubjectsTable)

    var name by SubjectsTable.name
    var abbreviation by SubjectsTable.abbreviation
    var credits by SubjectsTable.credits
    var obligation by SubjectsTable.obligation
    var completion by SubjectsTable.completion
    var faculty by SubjectsTable.faculty
    var semester by SubjectsTable.semester
    var year by SubjectsTable.year
}