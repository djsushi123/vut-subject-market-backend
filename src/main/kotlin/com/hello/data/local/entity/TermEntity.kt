package com.hello.data.local.entity

import com.hello.data.local.table.TermsTable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class TermEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<TermEntity>(TermsTable)

    var subjectId by TermsTable.subjectId
    var day by TermsTable.day
    var type by TermsTable.type
    var weeks by TermsTable.weeks
    var room by TermsTable.room
    var startTime by TermsTable.startTime
    var endTime by TermsTable.endTime
    var capacity by TermsTable.capacity
    var info by TermsTable.info
}