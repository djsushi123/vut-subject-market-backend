package com.hello.data.local.entity

import com.hello.data.local.table.UsersTable
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class UserEntity(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserEntity>(UsersTable)
    var email by UsersTable.email
    var password by UsersTable.password
    var username by UsersTable.username
}