package com.hello.data.local.table

import com.hello.domain.model.*
import kotlinx.serialization.SerialName
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.javatime.time
import java.time.LocalTime

object SportTermsTable : IntIdTable(name = "sport_terms") {
    val name: Column<String> = varchar("name", 100)
    val abbreviation: Column<String> = varchar("abbreviation", 15)
    val day: Column<Day> = enumeration("day")
    val startTime: Column<LocalTime> = time("start_time")
    val endTime: Column<LocalTime> = time("end_time")
    val weeks: Column<String> = varchar("weeks", 200)
    val teachingType: Column<TeachingType> = enumeration("teaching_type")
    val room: Column<String> = varchar("room", 50)
    val teacher: Column<String> = varchar("teacher", 100)
    val capacity: Column<Int?> = integer("capacity").nullable()
    val note: Column<String> = varchar("note", 500)
}