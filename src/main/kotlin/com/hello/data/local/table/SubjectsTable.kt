package com.hello.data.local.table

import com.hello.domain.model.Completion
import com.hello.domain.model.Faculty
import com.hello.domain.model.Obligation
import com.hello.domain.model.Semester
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object SubjectsTable : IntIdTable(name = "subjects") {
    val name: Column<String> = varchar("name", 100)
    val abbreviation: Column<String> = varchar("abbreviation", 20)
    val credits: Column<Int> = integer("credits")
    val obligation: Column<Obligation> = enumeration("obligation")
    val completion: Column<Completion> = enumeration("completion")
    val faculty: Column<Faculty> = enumeration("faculty")
    val semester: Column<Semester> = enumeration("semester")
    val year: Column<Int> = integer("year")
}