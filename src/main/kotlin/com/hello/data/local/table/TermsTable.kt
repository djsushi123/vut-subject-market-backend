package com.hello.data.local.table

import com.hello.domain.model.*
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.javatime.time
import java.time.LocalTime

object TermsTable : IntIdTable(name = "terms") {
    val subjectId = reference("subject_id", SubjectsTable)
    val day: Column<Day> = enumeration("day")
    val type: Column<TermType> = enumeration("type")
    val weeks: Column<String> = varchar("weeks", 200)
    val room: Column<String> = varchar("room", 100)
    val startTime: Column<LocalTime> = time("start_time")
    val endTime: Column<LocalTime> = time("end_time")
    val capacity: Column<Int?> = integer("capacity").nullable()
    val info: Column<String> = varchar("info", 500)
}