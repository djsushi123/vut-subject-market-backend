package com.hello.data.mapper

import com.hello.data.dto.CompletionDto
import com.hello.data.dto.ObligationDto
import com.hello.data.dto.UserDto
import com.hello.domain.model.Completion
import com.hello.domain.model.Obligation
import com.hello.domain.model.User

fun Completion.toDto() = when (this) {
    Completion.NONE -> CompletionDto.NONE
    Completion.CREDIT -> CompletionDto.CREDIT
    Completion.CREDIT_EXAM -> CompletionDto.CREDIT_EXAM
    Completion.EXAM -> CompletionDto.EXAM
    Completion.CLASSIFIED_CREDIT -> CompletionDto.CLASSIFIED_CREDIT
}

fun CompletionDto.toModel() = when (this) {
    CompletionDto.NONE -> Completion.NONE
    CompletionDto.CREDIT -> Completion.CREDIT
    CompletionDto.CREDIT_EXAM -> Completion.CREDIT_EXAM
    CompletionDto.EXAM -> Completion.EXAM
    CompletionDto.CLASSIFIED_CREDIT -> Completion.CLASSIFIED_CREDIT
}