package com.hello.data.mapper

import com.hello.data.dto.DayDto
import com.hello.data.dto.FacultyDto
import com.hello.data.dto.ObligationDto
import com.hello.data.dto.UserDto
import com.hello.domain.model.Day
import com.hello.domain.model.Faculty
import com.hello.domain.model.Obligation
import com.hello.domain.model.User

fun Day.toDto() = when (this) {
    Day.MONDAY -> DayDto.MONDAY
    Day.TUESDAY -> DayDto.TUESDAY
    Day.WEDNESDAY -> DayDto.WEDNESDAY
    Day.THURSDAY -> DayDto.THURSDAY
    Day.FRIDAY -> DayDto.FRIDAY
    Day.SATURDAY -> DayDto.SATURDAY
    Day.SUNDAY -> DayDto.SUNDAY
    Day.NONE -> DayDto.NONE
}

fun DayDto.toModel() = when (this) {
    DayDto.MONDAY -> Day.MONDAY
    DayDto.TUESDAY -> Day.TUESDAY
    DayDto.WEDNESDAY -> Day.WEDNESDAY
    DayDto.THURSDAY -> Day.THURSDAY
    DayDto.FRIDAY -> Day.FRIDAY
    DayDto.SATURDAY -> Day.SATURDAY
    DayDto.SUNDAY -> Day.SUNDAY
    DayDto.NONE -> Day.NONE
}