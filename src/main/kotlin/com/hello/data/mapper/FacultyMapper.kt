package com.hello.data.mapper

import com.hello.data.dto.FacultyDto
import com.hello.data.dto.ObligationDto
import com.hello.data.dto.UserDto
import com.hello.domain.model.Faculty
import com.hello.domain.model.Obligation
import com.hello.domain.model.User

fun Faculty.toDto() = when (this) {
    Faculty.FIT -> FacultyDto.FIT
    Faculty.FEKT -> FacultyDto.FEKT
    Faculty.ICV -> FacultyDto.ICV
    Faculty.FaVU -> FacultyDto.FaVU
    Faculty.FSI -> FacultyDto.FSI
    Faculty.FP -> FacultyDto.FP
    Faculty.FCH -> FacultyDto.FCH
    Faculty.CESA -> FacultyDto.CESA
    Faculty.USI -> FacultyDto.USI
    Faculty.FAST -> FacultyDto.FAST
    Faculty.FA -> FacultyDto.FA
    Faculty.CEITEC_VUT -> FacultyDto.CEITEC_VUT
}

fun FacultyDto.toModel() = when (this) {
    FacultyDto.FIT -> Faculty.FIT
    FacultyDto.FEKT -> Faculty.FEKT
    FacultyDto.ICV -> Faculty.ICV
    FacultyDto.FaVU -> Faculty.FaVU
    FacultyDto.FSI -> Faculty.FSI
    FacultyDto.FP -> Faculty.FP
    FacultyDto.FCH -> Faculty.FCH
    FacultyDto.CESA -> Faculty.CESA
    FacultyDto.USI -> Faculty.USI
    FacultyDto.FAST -> Faculty.FAST
    FacultyDto.FA -> Faculty.FA
    FacultyDto.CEITEC_VUT -> Faculty.CEITEC_VUT
}