package com.hello.data.mapper

import com.hello.data.dto.ObligationDto
import com.hello.data.dto.UserDto
import com.hello.domain.model.Obligation
import com.hello.domain.model.User

fun Obligation.toDto() = when (this) {
    Obligation.MANDATORY -> ObligationDto.MANDATORY
    Obligation.COMPULSORY -> ObligationDto.COMPULSORY
    Obligation.COMPULSORY_ENGLISH -> ObligationDto.COMPULSORY_ENGLISH
    Obligation.COMPULSORY_TECHNICAL -> ObligationDto.COMPULSORY_TECHNICAL
    Obligation.ELECTIVE -> ObligationDto.ELECTIVE
}

fun ObligationDto.toModel() = when (this) {
    ObligationDto.MANDATORY -> Obligation.MANDATORY
    ObligationDto.COMPULSORY -> Obligation.COMPULSORY
    ObligationDto.COMPULSORY_ENGLISH -> Obligation.COMPULSORY_ENGLISH
    ObligationDto.COMPULSORY_TECHNICAL -> Obligation.COMPULSORY_TECHNICAL
    ObligationDto.ELECTIVE -> Obligation.ELECTIVE
}