package com.hello.data.mapper

import com.hello.data.dto.SemesterDto
import com.hello.domain.model.Semester

fun Semester.toDto() = when (this) {
    Semester.WINTER -> SemesterDto.WINTER
    Semester.SUMMER -> SemesterDto.SUMMER
}

fun SemesterDto.toModel() = when (this) {
    SemesterDto.WINTER -> Semester.WINTER
    SemesterDto.SUMMER -> Semester.SUMMER
}

val x = Semester.WINTER.toDto()