package com.hello.data.mapper

import com.hello.data.dto.SportTermDto
import com.hello.data.local.entity.SportTermEntity
import com.hello.data.local.table.SportTermsTable
import com.hello.domain.model.SportTerm
import org.jetbrains.exposed.sql.ResultRow

fun SportTerm.toDto() = SportTermDto(
//    id = id,
    name = name,
    abbreviation = abbreviation,
    day = day.toDto(),
    startTime = startTime,
    endTime = endTime,
    teachingType = teachingType.toDto(),
    weeks = weeks,
    room = room,
    capacity = capacity?.toString(),
    teacher = teacher,
    note = note,
)

fun SportTermDto.toModel() = SportTerm(
//    id = id,
    name = name,
    day = day.toModel(),
    startTime = startTime,
    endTime = endTime,
    teachingType = teachingType.toModel(),
    weeks = weeks,
    room = room,
    capacity = capacity?.toIntOrNull(),
    teacher = teacher,
    abbreviation = abbreviation,
    note = note,
)

fun SportTermEntity.toModel() = SportTerm(
//    id = id.value,
    name = name,
    abbreviation = abbreviation,
    day = day,
    startTime = startTime,
    endTime = endTime,
    teachingType = teachingType,
    weeks = weeks,
    room = room,
    capacity = capacity,
    teacher = teacher,
    note = note,
)

fun ResultRow.toSportTermModel() = SportTerm(
//    id = this[SportTermsTable.id].value,
    name = this[SportTermsTable.name],
    abbreviation = this[SportTermsTable.abbreviation],
    day = this[SportTermsTable.day],
    startTime = this[SportTermsTable.startTime],
    endTime = this[SportTermsTable.endTime],
    teachingType = this[SportTermsTable.teachingType],
    weeks = this[SportTermsTable.weeks],
    room = this[SportTermsTable.room],
    teacher = this[SportTermsTable.teacher],
    capacity = this[SportTermsTable.capacity],
    note = this[SportTermsTable.note],
)