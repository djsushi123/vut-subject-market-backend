package com.hello.data.mapper

import com.hello.data.dto.SubjectDto
import com.hello.data.local.entity.SubjectEntity
import com.hello.data.local.table.SubjectsTable
import com.hello.domain.model.Subject
import org.jetbrains.exposed.sql.ResultRow

fun Subject.toDto() = SubjectDto(
    id = id,
    name = name,
    abbreviation = abbreviation,
    credits = credits,
    obligation = obligation.toDto(),
    completion = completion.toDto(),
    faculty = faculty.toDto(),
    semester = semester.toDto(),
    year = year
)

fun SubjectDto.toModel() = Subject(
    id = id,
    name = name,
    abbreviation = abbreviation,
    credits = credits,
    obligation = obligation.toModel(),
    completion = completion.toModel(),
    faculty = faculty.toModel(),
    semester = semester.toModel(),
    year = year
)

fun SubjectEntity.toModel() = Subject(
    id = id.value,
    name = name,
    abbreviation = abbreviation,
    credits = credits,
    obligation = obligation,
    completion = completion,
    faculty = faculty,
    semester = semester,
    year = year
)

fun ResultRow.toSubjectModel() = Subject(
    id = this[SubjectsTable.id].value,
    name = this[SubjectsTable.name],
    abbreviation = this[SubjectsTable.abbreviation],
    credits = this[SubjectsTable.credits],
    obligation = this[SubjectsTable.obligation],
    completion = this[SubjectsTable.completion],
    faculty = this[SubjectsTable.faculty],
    semester = this[SubjectsTable.semester],
    year = this[SubjectsTable.year]
)