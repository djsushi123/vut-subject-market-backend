package com.hello.data.mapper

import com.hello.data.dto.*
import com.hello.domain.model.*

fun TeachingType.toDto() = when (this) {
    TeachingType.TV_EXERCISE -> TeachingTypeDto.TV_EXERCISE
    TeachingType.TECHNOLOGICAL_EXERCISE -> TeachingTypeDto.TECHNOLOGICAL_EXERCISE
}

fun TeachingTypeDto.toModel() = when (this) {
    TeachingTypeDto.TV_EXERCISE -> TeachingType.TV_EXERCISE
    TeachingTypeDto.TECHNOLOGICAL_EXERCISE -> TeachingType.TECHNOLOGICAL_EXERCISE
}