package com.hello.data.mapper

import com.hello.data.dto.TermDto
import com.hello.data.local.entity.TermEntity
import com.hello.data.local.table.TermsTable
import com.hello.domain.model.Term
import org.jetbrains.exposed.sql.ResultRow

fun Term.toDto() = TermDto(
    id = id,
    subjectId = subjectId,
    day = day.toDto(),
    type = type.toDto(),
    weeks = weeks,
    room = room,
    startTime = startTime,
    endTime = endTime,
    capacity = capacity,
    info = info
)

fun TermDto.toModel() = Term(
    id = id,
    subjectId = subjectId,
    day = day.toModel(),
    type = type.toModel(),
    weeks = weeks,
    room = room,
    startTime = startTime,
    endTime = endTime,
    capacity = capacity,
    info = info
)

fun TermEntity.toModel() = Term(
    id = id.value,
    subjectId = subjectId.value,
    day = day,
    type = type,
    weeks = weeks,
    room = room,
    startTime = startTime,
    endTime = endTime,
    capacity = capacity,
    info = info
)

fun ResultRow.toTermModel() = Term(
    id = this[TermsTable.id].value,
    subjectId = this[TermsTable.subjectId].value,
    day = this[TermsTable.day],
    type = this[TermsTable.type],
    weeks = this[TermsTable.weeks],
    room = this[TermsTable.room],
    startTime = this[TermsTable.startTime],
    endTime = this[TermsTable.endTime],
    capacity = this[TermsTable.capacity],
    info = this[TermsTable.info]
)