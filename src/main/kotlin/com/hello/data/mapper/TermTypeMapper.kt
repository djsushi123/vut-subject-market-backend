package com.hello.data.mapper

import com.hello.data.dto.TermTypeDto
import com.hello.domain.model.TermType

fun TermType.toDto() = when (this) {
    TermType.EXERCISE -> TermTypeDto.EXERCISE
    TermType.LECTURE -> TermTypeDto.LECTURE
    TermType.LABORATORY -> TermTypeDto.LABORATORY
    TermType.COMPUTER_LABORATORY -> TermTypeDto.COMPUTER_LABORATORY
    TermType.OTHER -> TermTypeDto.OTHER
}

fun TermTypeDto.toModel() = when (this) {
    TermTypeDto.EXERCISE -> TermType.EXERCISE
    TermTypeDto.LECTURE -> TermType.LECTURE
    TermTypeDto.LABORATORY -> TermType.LABORATORY
    TermTypeDto.COMPUTER_LABORATORY -> TermType.COMPUTER_LABORATORY
    TermTypeDto.OTHER -> TermType.OTHER
}