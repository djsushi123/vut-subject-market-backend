package com.hello.data.mapper

import com.hello.data.dto.UserDto
import com.hello.domain.model.User

fun User.toDto() = UserDto(
    id = id,
    email = email,
    name = name,
    discordTag = discordTag
)

fun UserDto.toModel() = User(
    id = id,
    email = email,
    name = name,
    discordTag = discordTag
)