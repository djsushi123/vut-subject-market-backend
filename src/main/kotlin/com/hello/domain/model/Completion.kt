package com.hello.domain.model

enum class Completion {
    NONE,
    CREDIT,
    CREDIT_EXAM,
    EXAM,
    CLASSIFIED_CREDIT,
//    @SerialName("Colloquium") COLLOQUIUM TODO
}
