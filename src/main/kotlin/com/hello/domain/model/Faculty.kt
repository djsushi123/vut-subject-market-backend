package com.hello.domain.model

enum class Faculty {
    /**
     * Fakulta Informatiky
     */
    FIT,

    /**
     * Fakulta elektrotechniky
     */
    FEKT,

    /**
     * ICV FIXME
     */
    ICV,

    /**
     * Fakulta vytvarnych umeni
     */
    FaVU,

    /**
     * Fakulta strojniho inzenyrstvi
     */
    FSI,

    /**
     * Fakulta podnikatelska
     */
    FP,

    /**
     * Fakulta chemicka
     */
    FCH,

    /**
     * Centrum sportovnik aktivit
     */
    CESA,

    /**
     * Ustav soudniho inzenyrstvi
     */
    USI,

    /**
     * Fakulta stavebni
     */
    FAST,

    /**
     * Fakulta architektury
     */
    FA,

    /**
     * Stredoevropsky techonogicky institut VUT
     */
    CEITEC_VUT
}
