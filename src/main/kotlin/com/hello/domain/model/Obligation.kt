package com.hello.domain.model

enum class Obligation {
    MANDATORY,
    COMPULSORY,
    COMPULSORY_ENGLISH,
    COMPULSORY_TECHNICAL,
    ELECTIVE
}
