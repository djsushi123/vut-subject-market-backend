package com.hello.domain.model

enum class Semester {
    WINTER,
    SUMMER,
}
