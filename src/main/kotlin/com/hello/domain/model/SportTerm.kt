package com.hello.domain.model

import kotlinx.serialization.SerialName
import java.time.LocalTime


data class SportTerm(
//    val id: Int,
    val name: String,
    val abbreviation: String,
    val day: Day,
    val startTime: LocalTime,
    val endTime: LocalTime,
    val weeks: String,
    val teachingType: TeachingType,
    val room: String,
    val teacher: String,
    val capacity: Int?,
    val note: String
)