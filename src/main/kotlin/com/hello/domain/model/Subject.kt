package com.hello.domain.model

data class Subject(
    val id: Int,
    val name: String,
    val abbreviation: String,
    val credits: Int,
    val obligation: Obligation,
    val completion: Completion,
    val faculty: Faculty,
    val semester: Semester,
    val year: Int
)