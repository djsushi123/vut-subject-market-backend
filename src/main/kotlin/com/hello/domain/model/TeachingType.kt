package com.hello.domain.model

enum class TeachingType {
    TV_EXERCISE,
    TECHNOLOGICAL_EXERCISE,
}
