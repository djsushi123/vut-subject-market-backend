package com.hello.domain.model

import java.time.LocalTime


data class Term(
    val id: Int,
    val subjectId: Int,
    val day: Day,
    val type: TermType,
    val weeks: String,
    val room: String,
    val startTime: LocalTime,
    val endTime: LocalTime,
    val capacity: Int?,
    val info: String
)