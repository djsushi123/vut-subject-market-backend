package com.hello.domain.model

enum class TermType {
    EXERCISE,
    LECTURE,
    LABORATORY,
    COMPUTER_LABORATORY,
    OTHER
}
