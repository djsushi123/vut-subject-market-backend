package com.hello.domain.model

data class User(
    val id: Long,
    val email: String,
    val name: String,
    val discordTag: String
)