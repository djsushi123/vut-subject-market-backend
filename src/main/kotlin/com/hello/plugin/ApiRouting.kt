package com.hello.plugin

import io.ktor.server.routing.*

fun Routing.apiRouting() {
    route("/api/v1") {
        subjects()
        terms()
        sportTerms()
    }
}


