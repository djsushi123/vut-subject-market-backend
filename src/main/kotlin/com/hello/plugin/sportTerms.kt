package com.hello.plugin

import com.hello.data.local.entity.SportTermEntity
import com.hello.data.local.entity.TermEntity
import com.hello.data.local.table.SportTermsTable
import com.hello.data.local.table.TermsTable
import com.hello.data.mapper.toDto
import com.hello.data.mapper.toModel
import com.hello.data.mapper.toSportTermModel
import com.hello.data.mapper.toTermModel
import com.hello.normalize
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

fun Route.sportTerms() {
    route("/sport_terms") {
        get {
            val filter = call.request.queryParameters["filter"]?.normalize()
            println(filter)
            if (filter == null || filter.length < 3){
                call.respond(HttpStatusCode.BadRequest, "Filter needs to be at least 3 characters long", )
                return@get
            }
            val subjects = transaction {
                SportTermsTable.select {
                    (SportTermsTable.name like "%${filter}%") or (SportTermsTable.abbreviation like "%$filter%")
                }.map { it.toSportTermModel().toDto() }
            }
            call.respond(subjects)
        }
        get("/{id}") {
            val id = call.parameters["id"]?.toIntOrNull() ?: return@get
            val result = runCatching {
                transaction {
                    SportTermEntity[id].toModel().toDto()
                }
            }.getOrNull()

            if (result == null) {
                call.respond(HttpStatusCode.NoContent, "Fuck you")
                return@get
            }
            call.respond(result)
        }
    }
}