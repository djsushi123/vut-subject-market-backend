package com.hello.plugin

import com.hello.data.local.entity.SubjectEntity
import com.hello.data.local.table.SubjectsTable
import com.hello.data.mapper.toDto
import com.hello.data.mapper.toModel
import com.hello.data.mapper.toSubjectModel
import com.hello.normalize
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

fun Route.subjects() {
    route("/subjects") {
        get {
            val filter = call.request.queryParameters["filter"]?.normalize()
            val subjects = transaction {
                if (filter.isNullOrBlank()) SubjectEntity.all().map { it.toModel().toDto() }
                else SubjectsTable.select {
                    (SubjectsTable.name like "%$filter%") or (SubjectsTable.abbreviation like "%$filter%")
                }.map { it.toSubjectModel().toDto() }
            }
            call.respond(subjects)
        }
        get("/{id}") {
            val id = call.parameters["id"]?.toIntOrNull() ?: return@get
            val result = runCatching {
                transaction {
                    SubjectEntity[id].toModel().toDto()
                }
            }.getOrNull()

            if (result == null) {
                call.respond(status = HttpStatusCode.NoContent, message = "Fuck you")
                return@get
            }
            call.respond(result)
        }
    }
}