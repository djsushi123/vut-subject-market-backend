package com.hello.plugin

import com.hello.data.local.entity.TermEntity
import com.hello.data.local.table.TermsTable
import com.hello.data.mapper.toDto
import com.hello.data.mapper.toModel
import com.hello.data.mapper.toTermModel
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

fun Route.terms() {
    route("/terms") {
        get {
            val filter = call.request.queryParameters["subject_id"]?.toIntOrNull()
            val subjects = transaction {
                if (filter == null) TermEntity.all().map { it.toModel().toDto() }
                else TermsTable.select {
                    TermsTable.subjectId eq filter
                }.map { it.toTermModel().toDto() }
            }
            call.respond(subjects)
        }
        get("/{id}") {
            val id = call.parameters["id"]?.toIntOrNull() ?: return@get
            val result = runCatching {
                transaction {
                    TermEntity[id].toModel().toDto()
                }
            }.getOrNull()

            if (result == null) {
                call.respond(status = HttpStatusCode.NoContent, message = "Fuck you")
                return@get
            }
            call.respond(result)
        }
    }
}